package com.jenovaprojects.beanscard.updated_ui.database;

/**
 * Created by jenova on 5/30/17.
 */

public class Points {

    public int points;
    public String item;

    public Points(){}

    public Points(String item, int points) {
        this.points = points;
        this.item = item;
    }

    public int getPoints(){
        return points;
    }

    public void setPoints(int points){
        this.points = points;
    }

    public String getItem(){
        return item;
    }

    public void setItem(String item){
        this.item = item;
    }

}

