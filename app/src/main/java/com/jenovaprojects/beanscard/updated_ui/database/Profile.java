package com.jenovaprojects.beanscard.updated_ui.database;

/**
 * Created by jenova on 6/12/17.
 */

public class Profile {
    String name;

    public Profile(){}

    public Profile(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
