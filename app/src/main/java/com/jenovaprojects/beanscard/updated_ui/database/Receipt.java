package com.jenovaprojects.beanscard.updated_ui.database;

/**
 * Created by jenova on 5/27/17.
 */

public class Receipt {

    public int amount;
    public int points;

    public Receipt(){}

    public Receipt(int amount, int points) {
        this.amount = amount;
        this.points = points;
    }

    public int getAmount(){
        return amount;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public int getPoints(){
        return points;
    }

    public void setPoints(int points){
        this.points = points;
    }

}
