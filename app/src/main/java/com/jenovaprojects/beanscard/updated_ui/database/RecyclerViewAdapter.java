package com.jenovaprojects.beanscard.updated_ui.database;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jenovaprojects.beanscard.R;

import java.util.List;
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {
    private List<Points> task;
    protected Context context;
    public RecyclerViewAdapter(Context context, List<Points> task) {
        this.task = task;
        this.context = context;
    }
    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerViewHolders viewHolder = null;
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.to_do_list, parent, false);
        viewHolder = new RecyclerViewHolders(layoutView, task);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.categoryTitle.setText(String.valueOf(task.get(position).getPoints())+ " points");
        holder.itemTitle.setText(task.get(position).getItem());
    }
    @Override
    public int getItemCount() {
        return this.task.size();
    }
}