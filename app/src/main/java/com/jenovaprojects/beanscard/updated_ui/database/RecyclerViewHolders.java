package com.jenovaprojects.beanscard.updated_ui.database;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.jenovaprojects.beanscard.R;

import java.util.List;
public class RecyclerViewHolders extends RecyclerView.ViewHolder {
    private static final String TAG = RecyclerViewHolders.class.getSimpleName();
    public ImageView markIcon;
    public TextView categoryTitle, itemTitle;
    public ImageView deleteIcon;
    private List<Points> taskObject;

    public RecyclerViewHolders(final View itemView, final List<Points> taskObject) {
        super(itemView);
        this.taskObject = taskObject;
        categoryTitle = (TextView) itemView.findViewById(R.id.task_title);
        itemTitle = (TextView) itemView.findViewById(R.id.itemTV);
    }
}