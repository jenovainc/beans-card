package com.jenovaprojects.beanscard.updated_ui.main_fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.database.Points;
import com.jenovaprojects.beanscard.updated_ui.database.RecyclerViewAdapter;
import com.jenovaprojects.beanscard.updated_ui.login.LoginActivity;
import com.jenovaprojects.beanscard.updated_ui.login.SignupActivity;
import com.jenovaprojects.beanscard.updated_ui.ocr.BeansQRActivity;
import com.jenovaprojects.beanscard.updated_ui.settings.MoreInfoActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jenova on 5/19/17.
 */

public class DashboardFragment extends Fragment {

    private FirebaseAuth auth;
    private DatabaseReference mDatabase;

    FirebaseRemoteConfig mRemoteConfig;
    private static final String ContentLayout = "content_layout_enabled";
    private static final String Content1 = "content_1";
    private static final String Content2 = "content_2";

    private String mUserId, mUserToken, mUserName;
    private TextView pointTV;
    private int totalAmount;

    private ImageView moreInfo;

    private LinearLayout contentLayout;

    private TextView bev, lunch, dinner, mText, earned, content1, content2;

    private CardView freecoffee, freespecial, freelunch, freedinner;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseInstanceId mFirebaseInstId;

    private String LOG_TAG = "GenerateQRCode";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerViewAdapter recyclerViewAdapter;
    private List<Points> allPoints;

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard_main, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        RelativeLayout dash = (RelativeLayout) view.findViewById(R.id.dashView);
        RelativeLayout dashlogin = (RelativeLayout) view.findViewById(R.id.dashLogin);
        RelativeLayout tempscreen = (RelativeLayout) view.findViewById(R.id.tempScreen);

        freecoffee = (CardView) view.findViewById(R.id.coffeeRedeemCard);
        freespecial = (CardView) view.findViewById(R.id.specialtyRedeemCard);
        freelunch = (CardView) view.findViewById(R.id.lunchRedeemCard);
        freedinner = (CardView) view.findViewById(R.id.dinnerRedeemCard);

        moreInfo = (ImageView) view.findViewById(R.id.moreInfo);
        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MoreInfoActivity.class);
                startActivity(intent);
            }
        });

        dash.setVisibility(View.GONE);
        dashlogin.setVisibility(View.VISIBLE);

        Button login = (Button) view.findViewById(R.id.loginBtn);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SignupActivity.class);
                startActivity(intent);
            }
        });

        pointTV = (TextView) view.findViewById(R.id.pointsTV);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseInstId = FirebaseInstanceId.getInstance();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            //loadLogInView();
        } else {
            mUserId = mFirebaseUser.getUid();
            mUserToken = mFirebaseInstId.getToken();
            mUserName = mFirebaseUser.getDisplayName();

            Calendar c = Calendar.getInstance(); // Get current time
            int hr1 = c.get(Calendar.HOUR_OF_DAY); // Gets the current hour of the day from the calendar created ( from 1 to 24 )

            // mText = (TextView) view.findViewById(R.id.textView19);

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String str = sdf.format(new Date());

            String[] hr = str.split(":");

            if (hr1 < 12) {
                if (mUserName == null) {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Good Morning");
                } else {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Good Morning" + ", " + mUserName);
                }
            } else if (hr1 >= 12 && hr1 < 17) {
                if (mUserName == null) {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Good Afternoon");
                } else {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Good Afternoon" + ", " + mUserName);
                }
            } else if (hr1 >= 17 && hr1 < 20) {
                if (mUserName == null) {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Good Evening");
                } else {
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Good Evening" + ", " + mUserName);
                }
            }

            dash.setVisibility(View.VISIBLE);
            dashlogin.setVisibility(View.GONE);

            // Set up ListView
            // final ListView listView = (ListView) findViewById(R.id.listView);
            // final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1);
            // listView.setAdapter(adapter);

            //addListenerForSingleValueEvent

            DatabaseReference db_receipt = FirebaseDatabase.getInstance().getReference().child("user").child(mUserId);
            db_receipt.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long sum = 0;
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Points points = ds.getValue(Points.class);
                        sum = sum + points.getPoints();

                    }

                    pointTV.setText((int) sum + "");

                    checkText();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

           /* DatabaseReference list_db = FirebaseDatabase.getInstance().getReference().child("user").child(mUserId);
            list_db.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    getAllTask(dataSnapshot);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    getAllTask(dataSnapshot);
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });


            allPoints = new ArrayList<>();
            recyclerView = (RecyclerView) view.findViewById(R.id.point_list);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), allPoints);
            recyclerView.setAdapter(recyclerViewAdapter);*/

        }

          initRemoteConfig();


        return view;

    }

    private void getAllTask(DataSnapshot dataSnapshot) {
        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
            allPoints.add(singleSnapshot.getValue(Points.class));
        }
        recyclerViewAdapter.notifyDataSetChanged();
    }

    private void checkText() {

        String newest = pointTV.getText().toString();
        int newer = Integer.valueOf(newest);
        if (newer >= 65) {
            freecoffee.setVisibility(View.VISIBLE);
        }
        if (newer >= 115) {
            freespecial.setVisibility(View.VISIBLE);
        }
        if (newer >= 275) {
            freelunch.setVisibility(View.VISIBLE);
        }
        if (newer >= 425) {
            freedinner.setVisibility(View.VISIBLE);
        }
    }

      private void initRemoteConfig() {
        mRemoteConfig = FirebaseRemoteConfig.getInstance();

        Resources res = getResources();

        HashMap<String, Object> defaults = new HashMap<>();
        defaults.put("content_layout_enabled", res.getBoolean(R.bool.content_layout_enabled));
        defaults.put("content1", getString(R.string.content_1));
        defaults.put("content2", getString(R.string.content_2));


        mRemoteConfig.setDefaults(defaults);
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mRemoteConfig.setConfigSettings(remoteConfigSettings);
        fetchRemoteConfigValues();
    }

    private void fetchRemoteConfigValues() {
        long cacheExpiration = 3600;

        //expire the cache immediately for development mode.
        if (mRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            // task successful. Activate the fetched data
                            mRemoteConfig.activateFetched();

                        } else {
                            //task failed
                        }
                    }
                });
    }

}


