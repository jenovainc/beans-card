package com.jenovaprojects.beanscard.updated_ui.main_fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.HomeActivity;
import com.jenovaprojects.beanscard.updated_ui.database.Points;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jenova on 5/19/17.
 */

public class ScanFragment extends Fragment {

    FirebaseRemoteConfig mRemoteConfig;
    private static final String Featured_Name_1 = "featured_name_1";
    private static final String Featured_Name_2 = "featured_name_2";
    private static final String Featured_Name_3 = "featured_name_3";
    private static final String Featured_Name_4 = "featured_name_4";
    private static final String Featured_Name_5 = "featured_name_5";
    private static final String Featured_Desc_1 = "featured_desc_1";
    private static final String Featured_Desc_2 = "featured_desc_2";
    private static final String Featured_Desc_3 = "featured_desc_3";
    private static final String Featured_Desc_4 = "featured_desc_4";
    private static final String Featured_Desc_5 = "featured_desc_5";
    private static final String Featured_Card1 = "featured_card1";
    private static final String Featured_Card2 = "featured_card2";
    private static final String Featured_Card3 = "featured_card3";
    private static final String Featured_Card4 = "featured_card4";
    private static final String Featured_Card5 = "featured_card5";
    private static final String Featured_Header1 = "featured_header1";


    TextView featuredName1, featuredName2, featuredName3, featuredName4, featuredName5,
            featuredDesc1, featuredDesc2, featuredDesc3, featuredDesc4, featuredDesc5,
    featuredHeader1;
    CardView featuredCard1, featuredCard2, featuredCard3, featuredCard4, featuredCard5;

    public static ScanFragment newInstance() {
        ScanFragment fragment = new ScanFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scan_main, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Featured");

        featuredName1 = (TextView) view.findViewById(R.id.featured_name1);
        featuredName2 = (TextView) view.findViewById(R.id.featured_name2);
        featuredName3 = (TextView) view.findViewById(R.id.featured_name3);
        featuredName4 = (TextView) view.findViewById(R.id.featured_name4);
        featuredName5 = (TextView) view.findViewById(R.id.featured_name5);
        featuredDesc1 = (TextView) view.findViewById(R.id.featured_desc1);
        featuredDesc2 = (TextView) view.findViewById(R.id.featured_desc2);
        featuredDesc3 = (TextView) view.findViewById(R.id.featured_desc3);
        featuredDesc4 = (TextView) view.findViewById(R.id.featured_desc4);
        featuredDesc5 = (TextView) view.findViewById(R.id.featured_desc5);
        featuredHeader1 = (TextView) view.findViewById(R.id.featured_header1);

        featuredCard1 = (CardView) view.findViewById(R.id.featured_card1);
        featuredCard1.setVisibility(View.GONE);
        featuredCard2 = (CardView) view.findViewById(R.id.featured_card2);
        featuredCard2.setVisibility(View.GONE);
        featuredCard3 = (CardView) view.findViewById(R.id.featured_card3);
        featuredCard3.setVisibility(View.GONE);
        featuredCard4 = (CardView) view.findViewById(R.id.featured_card4);
        featuredCard4.setVisibility(View.GONE);
        featuredCard5 = (CardView) view.findViewById(R.id.featured_card5);
        featuredCard5.setVisibility(View.GONE);

        initRemoteConfig();
        setupView();

        return view;
    }

    private void initRemoteConfig() {
        mRemoteConfig = FirebaseRemoteConfig.getInstance();

        Resources res = getResources();

        HashMap<String, Object> defaults = new HashMap<>();
        defaults.put("Featured_Name_1", getString(R.string.Featured_Name_1));
        defaults.put("Featured_Name_2", getString(R.string.Featured_Name_2));
        defaults.put("Featured_Name_3", getString(R.string.Featured_Name_3));
        defaults.put("Featured_Name_4", getString(R.string.Featured_Name_4));
        defaults.put("Featured_Name_5", getString(R.string.Featured_Name_5));
        defaults.put("Featured_Desc_1", getString(R.string.Featured_Desc_1));
        defaults.put("Featured_Desc_2", getString(R.string.Featured_Desc_2));
        defaults.put("Featured_Desc_3", getString(R.string.Featured_Desc_3));
        defaults.put("Featured_Desc_4", getString(R.string.Featured_Desc_4));
        defaults.put("Featured_Desc_5", getString(R.string.Featured_Desc_5));
        defaults.put("Featured_Header1", getString(R.string.featured_header1));
        defaults.put("Featured_Card1", res.getBoolean(R.bool.featured_card1));
        defaults.put("Featured_Card2", res.getBoolean(R.bool.featured_card2));
        defaults.put("Featured_Card3", res.getBoolean(R.bool.featured_card3));
        defaults.put("Featured_Card4", res.getBoolean(R.bool.featured_card4));
        defaults.put("Featured_Card5", res.getBoolean(R.bool.featured_card5));

        mRemoteConfig.setDefaults(defaults);
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mRemoteConfig.setConfigSettings(remoteConfigSettings);
        fetchRemoteConfigValues();
    }

    private void fetchRemoteConfigValues() {
        long cacheExpiration = 3600;

        //expire the cache immediately for development mode.
        if (mRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            // task successful. Activate the fetched data
                            mRemoteConfig.activateFetched();
                            setupView();
                        } else {
                            //task failed
                        }
                    }
                });
    }

    private void setupView() {
        setFeatured_Name_1();
        setFeatured_Name_2();
        setFeatured_Name_3();
        setFeatured_Name_4();
        setFeatured_Name_5();
        setFeatured_Desc_1();
        setFeatured_Desc_2();
        setFeatured_Desc_3();
        setFeatured_Desc_4();
        setFeatured_Desc_5();
        setFeatured_Card1();
        setFeatured_Card2();
        setFeatured_Card3();
        setFeatured_Card4();
        setFeatured_Card5();
        setFeatured_Header1();

    }

    private void setFeatured_Card1() {
        boolean isPromoOn = mRemoteConfig.getBoolean(Featured_Card1);
        featuredCard1.setVisibility(isPromoOn ? View.VISIBLE : View.GONE);
    }

    private void setFeatured_Card2() {
        boolean isPromoOn = mRemoteConfig.getBoolean(Featured_Card2);
        featuredCard2.setVisibility(isPromoOn ? View.VISIBLE : View.GONE);
    }

    private void setFeatured_Card3() {
        boolean isPromoOn = mRemoteConfig.getBoolean(Featured_Card3);
        featuredCard3.setVisibility(isPromoOn ? View.VISIBLE : View.GONE);
    }

    private void setFeatured_Card4() {
        boolean isPromoOn = mRemoteConfig.getBoolean(Featured_Card4);
        featuredCard4.setVisibility(isPromoOn ? View.VISIBLE : View.GONE);
    }

    private void setFeatured_Card5() {
        boolean isPromoOn = mRemoteConfig.getBoolean(Featured_Card5);
        featuredCard5.setVisibility(isPromoOn ? View.VISIBLE : View.GONE);
    }

    private void setFeatured_Header1() {
        String prompt = mRemoteConfig.getString(Featured_Header1);
        if (prompt != null) {
            featuredHeader1.setText(prompt);
        }
    }

    private void setFeatured_Name_1() {
        String prompt = mRemoteConfig.getString(Featured_Name_1);
        if (prompt != null) {
            featuredName1.setText(prompt);
        }
    }

    private void setFeatured_Name_2() {
        String prompt = mRemoteConfig.getString(Featured_Name_2);
        if (prompt != null) {
            featuredName2.setText(prompt);
        }
    }

    private void setFeatured_Name_3() {
        String prompt = mRemoteConfig.getString(Featured_Name_3);
        if (prompt != null) {
            featuredName3.setText(prompt);
        }
    }

    private void setFeatured_Name_4() {
        String prompt = mRemoteConfig.getString(Featured_Name_4);
        if (prompt != null) {
            featuredName4.setText(prompt);
        }
    }

    private void setFeatured_Name_5() {
        String prompt = mRemoteConfig.getString(Featured_Name_5);
        if (prompt != null) {
            featuredName5.setText(prompt);
        }
    }

    private void setFeatured_Desc_1() {
        String prompt = mRemoteConfig.getString(Featured_Desc_1);
        if (prompt != null) {
            featuredDesc1.setText(prompt);
        }
    }

    private void setFeatured_Desc_2() {
        String prompt = mRemoteConfig.getString(Featured_Desc_2);
        if (prompt != null) {
            featuredDesc2.setText(prompt);
        }
    }

    private void setFeatured_Desc_3() {
        String prompt = mRemoteConfig.getString(Featured_Desc_3);
        if (prompt != null) {
            featuredDesc3.setText(prompt);
        }
    }

    private void setFeatured_Desc_4() {
        String prompt = mRemoteConfig.getString(Featured_Desc_4);
        if (prompt != null) {
            featuredDesc4.setText(prompt);
        }
    }

    private void setFeatured_Desc_5() {
        String prompt = mRemoteConfig.getString(Featured_Desc_5);
        if (prompt != null) {
            featuredDesc5.setText(prompt);
        }
    }

}