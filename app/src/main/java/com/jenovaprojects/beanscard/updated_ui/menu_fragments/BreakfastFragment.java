package com.jenovaprojects.beanscard.updated_ui.menu_fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.main_fragments.HomeFragment;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.Menu;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.MenusAdapter;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.menu_decorators.DividerItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jenova on 5/20/17.
 */

public class BreakfastFragment extends Fragment {

    FirebaseRemoteConfig mRemoteConfig;
    private static final String BreakfastItem1 = "breakfast_item_1";
    private static final String BreakfastItem2 = "breakfast_item_2";
    private static final String BreakfastItem3 = "breakfast_item_3";
    private static final String BreakfastItem4 = "breakfast_item_4";
    private static final String BreakfastItem5 = "breakfast_item_5";
    private static final String BreakfastItem6 = "breakfast_item_6";
    private static final String BreakfastItem7 = "breakfast_item_7";
    private static final String BreakfastItem8 = "breakfast_item_8";
    private static final String BreakfastItem9 = "breakfast_item_9";
    private static final String BreakfastItem10 = "breakfast_item_10";
    private static final String BreakfastItem11 = "breakfast_item_11";
    private static final String BreakfastItem12 = "breakfast_item_12";

    private static final String BreakfastDesc1 = "breakfast_desc_1";
    private static final String BreakfastDesc2 = "breakfast_desc_2";
    private static final String BreakfastDesc3 = "breakfast_desc_3";
    private static final String BreakfastDesc4 = "breakfast_desc_4";
    private static final String BreakfastDesc5 = "breakfast_desc_5";
    private static final String BreakfastDesc6 = "breakfast_desc_6";
    private static final String BreakfastDesc7 = "breakfast_desc_7";
    private static final String BreakfastDesc8 = "breakfast_desc_8";
    private static final String BreakfastDesc9 = "breakfast_desc_9";
    private static final String BreakfastDesc10 = "breakfast_desc_10";
    private static final String BreakfastDesc11 = "breakfast_desc_11";
    private static final String BreakfastDesc12 = "breakfast_desc_12";

    private static final String BreakfastPrice1 = "breakfast_price_1";
    private static final String BreakfastPrice2 = "breakfast_price_2";
    private static final String BreakfastPrice3 = "breakfast_price_3";
    private static final String BreakfastPrice4 = "breakfast_price_4";
    private static final String BreakfastPrice5 = "breakfast_price_5";
    private static final String BreakfastPrice6 = "breakfast_price_6";
    private static final String BreakfastPrice7 = "breakfast_price_7";
    private static final String BreakfastPrice8 = "breakfast_price_8";
    private static final String BreakfastPrice9 = "breakfast_price_9";
    private static final String BreakfastPrice10 = "breakfast_price_10";
    private static final String BreakfastPrice11 = "breakfast_price_11";
    private static final String BreakfastPrice12 = "breakfast_price_12";

    TextView breakfastItem1, breakfastItem2, breakfastItem3, breakfastItem4, breakfastItem5,
            breakfastItem6, breakfastItem7, breakfastItem8, breakfastItem9, breakfastItem10,
            breakfastItem11, breakfastItem12, breakfastDesc1, breakfastDesc2, breakfastDesc3,
            breakfastDesc4, breakfastDesc5, breakfastDesc6, breakfastDesc7, breakfastDesc8,
            breakfastDesc9, breakfastDesc10, breakfastDesc11, breakfastDesc12, breakfastPrice1,
            breakfastPrice2, breakfastPrice3, breakfastPrice4, breakfastPrice5, breakfastPrice6,
            breakfastPrice7, breakfastPrice8, breakfastPrice9, breakfastPrice10, breakfastPrice11,
            breakfastPrice12;

    public static BreakfastFragment newInstance() {
        BreakfastFragment fragment = new BreakfastFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_breakfast, container, false);

        breakfastDesc1 = (TextView) view.findViewById(R.id.breakast_desc_1);
        breakfastDesc2 = (TextView) view.findViewById(R.id.breakast_desc_2);
        breakfastDesc3 = (TextView) view.findViewById(R.id.breakast_desc_3);
        breakfastDesc4 = (TextView) view.findViewById(R.id.breakast_desc_4);
        breakfastDesc5 = (TextView) view.findViewById(R.id.breakast_desc_5);
        breakfastDesc6 = (TextView) view.findViewById(R.id.breakast_desc_6);
        breakfastDesc7 = (TextView) view.findViewById(R.id.breakast_desc_7);
        breakfastDesc8 = (TextView) view.findViewById(R.id.breakast_desc_8);
        breakfastDesc9 = (TextView) view.findViewById(R.id.breakast_desc_9);
        breakfastDesc10 = (TextView) view.findViewById(R.id.breakast_desc_10);
        breakfastDesc11 = (TextView) view.findViewById(R.id.breakast_desc_11);
        breakfastDesc12 = (TextView) view.findViewById(R.id.breakast_desc_12);
        breakfastItem1 = (TextView) view.findViewById(R.id.breakast_item_1);
        breakfastItem2 = (TextView) view.findViewById(R.id.breakast_item_2);
        breakfastItem3 = (TextView) view.findViewById(R.id.breakast_item_3);
        breakfastItem4 = (TextView) view.findViewById(R.id.breakast_item_4);
        breakfastItem5 = (TextView) view.findViewById(R.id.breakast_item_5);
        breakfastItem6 = (TextView) view.findViewById(R.id.breakast_item_6);
        breakfastItem7 = (TextView) view.findViewById(R.id.breakast_item_7);
        breakfastItem8 = (TextView) view.findViewById(R.id.breakast_item_8);
        breakfastItem9 = (TextView) view.findViewById(R.id.breakast_item_9);
        breakfastItem10 = (TextView) view.findViewById(R.id.breakast_item_10);
        breakfastItem11 = (TextView) view.findViewById(R.id.breakast_item_11);
        breakfastItem12 = (TextView) view.findViewById(R.id.breakast_item_12);
        breakfastPrice1 = (TextView) view.findViewById(R.id.breakast_price_1);
        breakfastPrice2 = (TextView) view.findViewById(R.id.breakast_price_2);
        breakfastPrice3 = (TextView) view.findViewById(R.id.breakast_price_3);
        breakfastPrice4 = (TextView) view.findViewById(R.id.breakast_price_4);
        breakfastPrice5 = (TextView) view.findViewById(R.id.breakast_price_5);
        breakfastPrice6 = (TextView) view.findViewById(R.id.breakast_price_6);
        breakfastPrice7 = (TextView) view.findViewById(R.id.breakast_price_7);
        breakfastPrice8 = (TextView) view.findViewById(R.id.breakast_price_8);
        breakfastPrice9 = (TextView) view.findViewById(R.id.breakast_price_9);
        breakfastPrice10 = (TextView) view.findViewById(R.id.breakast_price_10);
        breakfastPrice11 = (TextView) view.findViewById(R.id.breakast_price_11);
        breakfastPrice12 = (TextView) view.findViewById(R.id.breakast_price_12);

        initRemoteConfig();
        setupView();

        return view;
    }

    private void initRemoteConfig() {
        mRemoteConfig = FirebaseRemoteConfig.getInstance();

        Resources res = getResources();

        HashMap<String, Object> defaults = new HashMap<>();
        defaults.put("Breakfast_Item_1", getString(R.string.Breakfast_Item_1));
        defaults.put("Breakfast_Item_2", getString(R.string.Breakfast_Item_2));
        defaults.put("Breakfast_Item_3", getString(R.string.Breakfast_Item_3));
        defaults.put("Breakfast_Item_4", getString(R.string.Breakfast_Item_4));
        defaults.put("Breakfast_Item_5", getString(R.string.Breakfast_Item_5));
        defaults.put("Breakfast_Item_6", getString(R.string.Breakfast_Item_6));
        defaults.put("Breakfast_Item_7", getString(R.string.Breakfast_Item_7));
        defaults.put("Breakfast_Item_8", getString(R.string.Breakfast_Item_8));
        defaults.put("Breakfast_Item_9", getString(R.string.Breakfast_Item_9));
        defaults.put("Breakfast_Item_10", getString(R.string.Breakfast_Item_10));
        defaults.put("Breakfast_Item_11", getString(R.string.Breakfast_Item_11));
        defaults.put("Breakfast_Item_12", getString(R.string.Breakfast_Item_12));
        defaults.put("Breakfast_Desc_1", getString(R.string.Breakfast_Desc_1));
        defaults.put("Breakfast_Desc_2", getString(R.string.Breakfast_Desc_2));
        defaults.put("Breakfast_Desc_3", getString(R.string.Breakfast_Desc_3));
        defaults.put("Breakfast_Desc_4", getString(R.string.Breakfast_Desc_4));
        defaults.put("Breakfast_Desc_5", getString(R.string.Breakfast_Desc_5));
        defaults.put("Breakfast_Desc_6", getString(R.string.Breakfast_Desc_6));
        defaults.put("Breakfast_Desc_7", getString(R.string.Breakfast_Desc_7));
        defaults.put("Breakfast_Desc_8", getString(R.string.Breakfast_Desc_8));
        defaults.put("Breakfast_Desc_9", getString(R.string.Breakfast_Desc_9));
        defaults.put("Breakfast_Desc_10", getString(R.string.Breakfast_Desc_10));
        defaults.put("Breakfast_Desc_11", getString(R.string.Breakfast_Desc_11));
        defaults.put("Breakfast_Desc_12", getString(R.string.Breakfast_Desc_12));
        defaults.put("Breakfast_Price_1", getString(R.string.Breakfast_Price_1));
        defaults.put("Breakfast_Price_2", getString(R.string.Breakfast_Price_2));
        defaults.put("Breakfast_Price_3", getString(R.string.Breakfast_Price_3));
        defaults.put("Breakfast_Price_4", getString(R.string.Breakfast_Price_4));
        defaults.put("Breakfast_Price_5", getString(R.string.Breakfast_Price_5));
        defaults.put("Breakfast_Price_6", getString(R.string.Breakfast_Price_6));
        defaults.put("Breakfast_Price_7", getString(R.string.Breakfast_Price_7));
        defaults.put("Breakfast_Price_8", getString(R.string.Breakfast_Price_8));
        defaults.put("Breakfast_Price_9", getString(R.string.Breakfast_Price_9));
        defaults.put("Breakfast_Price_10", getString(R.string.Breakfast_Price_10));
        defaults.put("Breakfast_Price_11", getString(R.string.Breakfast_Price_11));
        defaults.put("Breakfast_Price_12", getString(R.string.Breakfast_Price_12));


        mRemoteConfig.setDefaults(defaults);
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mRemoteConfig.setConfigSettings(remoteConfigSettings);
        fetchRemoteConfigValues();
    }

    private void fetchRemoteConfigValues() {
        long cacheExpiration = 3600;

        //expire the cache immediately for development mode.
        if (mRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            // task successful. Activate the fetched data
                            mRemoteConfig.activateFetched();
                            setupView();
                        } else {
                            //task failed
                        }
                    }
                });
    }

    private void setupView() {
        setBreakfastItem1();
        setBreakfastItem2();
        setBreakfastItem3();
        setBreakfastItem4();
        setBreakfastItem5();
        setBreakfastItem6();
        setBreakfastItem7();
        setBreakfastItem8();
        setBreakfastItem9();
        setBreakfastItem10();
        setBreakfastItem11();
        setBreakfastItem12();
        setBreakfastDesc1();
        setBreakfastDesc2();
        setBreakfastDesc3();
        setBreakfastDesc4();
        setBreakfastDesc5();
        setBreakfastDesc6();
        setBreakfastDesc7();
        setBreakfastDesc8();
        setBreakfastDesc9();
        setBreakfastDesc10();
        setBreakfastDesc11();
        setBreakfastDesc12();
        setBreakfastPrice1();
        setBreakfastPrice2();
        setBreakfastPrice3();
        setBreakfastPrice4();
        setBreakfastPrice5();
        setBreakfastPrice6();
        setBreakfastPrice7();
        setBreakfastPrice8();
        setBreakfastPrice9();
        setBreakfastPrice10();
        setBreakfastPrice11();
        setBreakfastPrice12();
    }

    private void setBreakfastItem1() {
        String prompt = mRemoteConfig.getString(BreakfastItem1);
        if (prompt != null) {
            breakfastItem1.setText(prompt);
        }
    }

    private void setBreakfastItem2() {
        String prompt = mRemoteConfig.getString(BreakfastItem2);
        if (prompt != null) {
            breakfastItem2.setText(prompt);
        }
    }

    private void setBreakfastItem3() {
        String prompt = mRemoteConfig.getString(BreakfastItem3);
        if (prompt != null) {
            breakfastItem3.setText(prompt);
        }
    }

    private void setBreakfastItem4() {
        String prompt = mRemoteConfig.getString(BreakfastItem4);
        if (prompt != null) {
            breakfastItem4.setText(prompt);
        }
    }

    private void setBreakfastItem5() {
        String prompt = mRemoteConfig.getString(BreakfastItem5);
        if (prompt != null) {
            breakfastItem5.setText(prompt);
        }
    }

    private void setBreakfastItem6() {
        String prompt = mRemoteConfig.getString(BreakfastItem6);
        if (prompt != null) {
            breakfastItem6.setText(prompt);
        }
    }

    private void setBreakfastItem7() {
        String prompt = mRemoteConfig.getString(BreakfastItem7);
        if (prompt != null) {
            breakfastItem7.setText(prompt);
        }
    }

    private void setBreakfastItem8() {
        String prompt = mRemoteConfig.getString(BreakfastItem8);
        if (prompt != null) {
            breakfastItem8.setText(prompt);
        }
    }

    private void setBreakfastItem9() {
        String prompt = mRemoteConfig.getString(BreakfastItem9);
        if (prompt != null) {
            breakfastItem9.setText(prompt);
        }
    }

    private void setBreakfastItem10() {
        String prompt = mRemoteConfig.getString(BreakfastItem10);
        if (prompt != null) {
            breakfastItem10.setText(prompt);
        }
    }

    private void setBreakfastItem11() {
        String prompt = mRemoteConfig.getString(BreakfastItem11);
        if (prompt != null) {
            breakfastItem11.setText(prompt);
        }
    }

    private void setBreakfastItem12() {
        String prompt = mRemoteConfig.getString(BreakfastItem12);
        if (prompt != null) {
            breakfastItem12.setText(prompt);
        }
    }

    private void setBreakfastDesc1() {
        String prompt = mRemoteConfig.getString(BreakfastDesc1);
        if (prompt != null) {
            breakfastDesc1.setText(prompt);
        }
    }

    private void setBreakfastDesc2() {
        String prompt = mRemoteConfig.getString(BreakfastDesc2);
        if (prompt != null) {
            breakfastDesc2.setText(prompt);
        }
    }
    private void setBreakfastDesc3() {
        String prompt = mRemoteConfig.getString(BreakfastDesc3);
        if (prompt != null) {
            breakfastDesc3.setText(prompt);
        }
    }
    private void setBreakfastDesc4() {
        String prompt = mRemoteConfig.getString(BreakfastDesc4);
        if (prompt != null) {
            breakfastDesc4.setText(prompt);
        }
    }
    private void setBreakfastDesc5() {
        String prompt = mRemoteConfig.getString(BreakfastDesc5);
        if (prompt != null) {
            breakfastDesc5.setText(prompt);
        }
    }
    private void setBreakfastDesc6() {
        String prompt = mRemoteConfig.getString(BreakfastDesc6);
        if (prompt != null) {
            breakfastDesc6.setText(prompt);
        }
    }
    private void setBreakfastDesc7() {
        String prompt = mRemoteConfig.getString(BreakfastDesc7);
        if (prompt != null) {
            breakfastDesc7.setText(prompt);
        }
    }
    private void setBreakfastDesc8() {
        String prompt = mRemoteConfig.getString(BreakfastDesc8);
        if (prompt != null) {
            breakfastDesc8.setText(prompt);
        }
    }
    private void setBreakfastDesc9() {
        String prompt = mRemoteConfig.getString(BreakfastDesc9);
        if (prompt != null) {
            breakfastDesc9.setText(prompt);
        }
    }
    private void setBreakfastDesc10() {
        String prompt = mRemoteConfig.getString(BreakfastDesc10);
        if (prompt != null) {
            breakfastDesc10.setText(prompt);
        }
    }
    private void setBreakfastDesc11() {
        String prompt = mRemoteConfig.getString(BreakfastDesc11);
        if (prompt != null) {
            breakfastDesc11.setText(prompt);
        }
    }
    private void setBreakfastDesc12() {
        String prompt = mRemoteConfig.getString(BreakfastDesc12);
        if (prompt != null) {
            breakfastDesc12.setText(prompt);
        }
    }

    private void setBreakfastPrice1() {
        String prompt = mRemoteConfig.getString(BreakfastPrice1);
        if (prompt != null) {
            breakfastPrice1.setText(prompt);
        }
    }
    private void setBreakfastPrice2() {
        String prompt = mRemoteConfig.getString(BreakfastPrice2);
        if (prompt != null) {
            breakfastPrice2.setText(prompt);
        }
    }

    private void setBreakfastPrice3() {
        String prompt = mRemoteConfig.getString(BreakfastPrice3);
        if (prompt != null) {
            breakfastPrice3.setText(prompt);
        }
    }

    private void setBreakfastPrice4() {
        String prompt = mRemoteConfig.getString(BreakfastPrice4);
        if (prompt != null) {
            breakfastPrice4.setText(prompt);
        }
    }

    private void setBreakfastPrice5() {
        String prompt = mRemoteConfig.getString(BreakfastPrice5);
        if (prompt != null) {
            breakfastPrice5.setText(prompt);
        }
    }

    private void setBreakfastPrice6() {
        String prompt = mRemoteConfig.getString(BreakfastPrice6);
        if (prompt != null) {
            breakfastPrice6.setText(prompt);
        }
    }
    private void setBreakfastPrice7() {
        String prompt = mRemoteConfig.getString(BreakfastPrice7);
        if (prompt != null) {
            breakfastPrice7.setText(prompt);
        }
    }

    private void setBreakfastPrice8() {
        String prompt = mRemoteConfig.getString(BreakfastPrice8);
        if (prompt != null) {
            breakfastPrice8.setText(prompt);
        }
    }

    private void setBreakfastPrice9() {
        String prompt = mRemoteConfig.getString(BreakfastPrice9);
        if (prompt != null) {
            breakfastPrice9.setText(prompt);
        }
    }

    private void setBreakfastPrice10() {
        String prompt = mRemoteConfig.getString(BreakfastPrice10);
        if (prompt != null) {
            breakfastPrice10.setText(prompt);
        }
    }

    private void setBreakfastPrice11() {
        String prompt = mRemoteConfig.getString(BreakfastPrice11);
        if (prompt != null) {
            breakfastPrice11.setText(prompt);
        }
    }

    private void setBreakfastPrice12() {
        String prompt = mRemoteConfig.getString(BreakfastPrice12);
        if (prompt != null) {
            breakfastPrice12.setText(prompt);
        }
    }




}
