package com.jenovaprojects.beanscard.updated_ui.menu_fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.Menu;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.MenusAdapter;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.menu_decorators.DividerItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jenova on 5/20/17.
 */

public class DinnerFragment extends Fragment {

    FirebaseRemoteConfig mRemoteConfig;
    private static final String DinnerItem1 = "dinner_item_1";
    private static final String DinnerItem2 = "dinner_item_2";
    private static final String DinnerItem3 = "dinner_item_3";
    private static final String DinnerItem4 = "dinner_item_4";
    private static final String DinnerItem5 = "dinner_item_5";
    private static final String DinnerItem6 = "dinner_item_6";
    private static final String DinnerItem7 = "dinner_item_7";
    private static final String DinnerItem8 = "dinner_item_8";
    private static final String DinnerItem9 = "dinner_item_9";
    private static final String DinnerItem10 = "dinner_item_10";
    private static final String DinnerItem11 = "dinner_item_11";
    private static final String DinnerItem12 = "dinner_item_12";
    private static final String DinnerItem13 = "dinner_item_13";
    private static final String DinnerItem14 = "dinner_item_14";
    private static final String DinnerItem15 = "dinner_item_15";
    private static final String DinnerItem16 = "dinner_item_16";
    private static final String DinnerItem17 = "dinner_item_17";
    private static final String DinnerItem18 = "dinner_item_18";
    private static final String DinnerItem19 = "dinner_item_19";
    private static final String DinnerItem20 = "dinner_item_20";
    private static final String DinnerItem21 = "dinner_item_21";

    private static final String DinnerDesc1 = "dinner_desc_1";
    private static final String DinnerDesc2 = "dinner_desc_2";
    private static final String DinnerDesc3 = "dinner_desc_3";
    private static final String DinnerDesc4 = "dinner_desc_4";
    private static final String DinnerDesc5 = "dinner_desc_5";
    private static final String DinnerDesc6 = "dinner_desc_6";
    private static final String DinnerDesc7 = "dinner_desc_7";
    private static final String DinnerDesc8 = "dinner_desc_8";
    private static final String DinnerDesc9 = "dinner_desc_9";
    private static final String DinnerDesc10 = "dinner_desc_10";
    private static final String DinnerDesc11 = "dinner_desc_11";
    private static final String DinnerDesc12 = "dinner_desc_12";
    private static final String DinnerDesc13 = "dinner_desc_13";
    private static final String DinnerDesc14 = "dinner_desc_14";
    private static final String DinnerDesc15 = "dinner_desc_15";
    private static final String DinnerDesc16 = "dinner_desc_16";
    private static final String DinnerDesc17 = "dinner_desc_17";
    private static final String DinnerDesc18 = "dinner_desc_18";
    private static final String DinnerDesc19 = "dinner_desc_19";
    private static final String DinnerDesc20 = "dinner_desc_20";
    private static final String DinnerDesc21 = "dinner_desc_21";

    private static final String DinnerPrice1 = "dinner_price_1";
    private static final String DinnerPrice2 = "dinner_price_2";
    private static final String DinnerPrice3 = "dinner_price_3";
    private static final String DinnerPrice4 = "dinner_price_4";
    private static final String DinnerPrice5 = "dinner_price_5";
    private static final String DinnerPrice6 = "dinner_price_6";
    private static final String DinnerPrice7 = "dinner_price_7";
    private static final String DinnerPrice8 = "dinner_price_8";
    private static final String DinnerPrice9 = "dinner_price_9";
    private static final String DinnerPrice10 = "dinner_price_10";
    private static final String DinnerPrice11 = "dinner_price_11";
    private static final String DinnerPrice12 = "dinner_price_12";
    private static final String DinnerPrice13 = "dinner_price_13";
    private static final String DinnerPrice14 = "dinner_price_14";
    private static final String DinnerPrice15 = "dinner_price_15";
    private static final String DinnerPrice16 = "dinner_price_16";
    private static final String DinnerPrice17 = "dinner_price_17";
    private static final String DinnerPrice18 = "dinner_price_18";
    private static final String DinnerPrice19 = "dinner_price_19";
    private static final String DinnerPrice20 = "dinner_price_20";
    private static final String DinnerPrice21 = "dinner_price_21";

    TextView dinnerItem1, dinnerItem2, dinnerItem3, dinnerItem4, dinnerItem5,
            dinnerItem6, dinnerItem7, dinnerItem8, dinnerItem9, dinnerItem10,
            dinnerItem11, dinnerItem12, dinnerItem13, dinnerItem14, dinnerItem15, dinnerItem16,
            dinnerItem17, dinnerItem18, dinnerItem19, dinnerItem20, dinnerItem21,
            dinnerDesc1, dinnerDesc2, dinnerDesc3, dinnerDesc4, dinnerDesc5, dinnerDesc6, dinnerDesc7,
            dinnerDesc8, dinnerDesc9, dinnerDesc10, dinnerDesc11, dinnerDesc12, dinnerDesc13,
            dinnerDesc14, dinnerDesc15, dinnerDesc16, dinnerDesc17, dinnerDesc18, dinnerDesc19,
            dinnerDesc20, dinnerDesc21,
            dinnerPrice1, dinnerPrice2, dinnerPrice3, dinnerPrice4, dinnerPrice5, dinnerPrice6,
            dinnerPrice7, dinnerPrice8, dinnerPrice9, dinnerPrice10, dinnerPrice11,
            dinnerPrice12, dinnerPrice13, dinnerPrice14, dinnerPrice15, dinnerPrice16,
            dinnerPrice17, dinnerPrice18, dinnerPrice19, dinnerPrice20, dinnerPrice21;

    public static DinnerFragment newInstance() {
        DinnerFragment fragment = new DinnerFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dinner, container, false);

        dinnerDesc1 = (TextView) view.findViewById(R.id.dinner_desc_1);
        dinnerDesc2 = (TextView) view.findViewById(R.id.dinner_desc_2);
        dinnerDesc3 = (TextView) view.findViewById(R.id.dinner_desc_3);
        dinnerDesc4 = (TextView) view.findViewById(R.id.dinner_desc_4);
        dinnerDesc5 = (TextView) view.findViewById(R.id.dinner_desc_5);
        dinnerDesc6 = (TextView) view.findViewById(R.id.dinner_desc_6);
        dinnerDesc7 = (TextView) view.findViewById(R.id.dinner_desc_7);
        dinnerDesc8 = (TextView) view.findViewById(R.id.dinner_desc_8);
        dinnerDesc9 = (TextView) view.findViewById(R.id.dinner_desc_9);
        dinnerDesc10 = (TextView) view.findViewById(R.id.dinner_desc_10);
        dinnerDesc11 = (TextView) view.findViewById(R.id.dinner_desc_11);
        dinnerDesc12 = (TextView) view.findViewById(R.id.dinner_desc_12);
        dinnerDesc13 = (TextView) view.findViewById(R.id.dinner_desc_13);
        dinnerDesc14 = (TextView) view.findViewById(R.id.dinner_desc_14);
        dinnerDesc15 = (TextView) view.findViewById(R.id.dinner_desc_15);
        dinnerDesc16 = (TextView) view.findViewById(R.id.dinner_desc_16);
        dinnerDesc17 = (TextView) view.findViewById(R.id.dinner_desc_17);
        dinnerDesc18 = (TextView) view.findViewById(R.id.dinner_desc_18);
        dinnerDesc19 = (TextView) view.findViewById(R.id.dinner_desc_19);
        dinnerDesc20 = (TextView) view.findViewById(R.id.dinner_desc_20);
        dinnerDesc21 = (TextView) view.findViewById(R.id.dinner_desc_21);
        dinnerItem1 = (TextView) view.findViewById(R.id.dinner_item_1);
        dinnerItem2 = (TextView) view.findViewById(R.id.dinner_item_2);
        dinnerItem3 = (TextView) view.findViewById(R.id.dinner_item_3);
        dinnerItem4 = (TextView) view.findViewById(R.id.dinner_item_4);
        dinnerItem5 = (TextView) view.findViewById(R.id.dinner_item_5);
        dinnerItem6 = (TextView) view.findViewById(R.id.dinner_item_6);
        dinnerItem7 = (TextView) view.findViewById(R.id.dinner_item_7);
        dinnerItem8 = (TextView) view.findViewById(R.id.dinner_item_8);
        dinnerItem9 = (TextView) view.findViewById(R.id.dinner_item_9);
        dinnerItem10 = (TextView) view.findViewById(R.id.dinner_item_10);
        dinnerItem11 = (TextView) view.findViewById(R.id.dinner_item_11);
        dinnerItem12 = (TextView) view.findViewById(R.id.dinner_item_12);
        dinnerItem13 = (TextView) view.findViewById(R.id.dinner_item_13);
        dinnerItem14 = (TextView) view.findViewById(R.id.dinner_item_14);
        dinnerItem15 = (TextView) view.findViewById(R.id.dinner_item_15);
        dinnerItem16 = (TextView) view.findViewById(R.id.dinner_item_16);
        dinnerItem17 = (TextView) view.findViewById(R.id.dinner_item_17);
        dinnerItem18 = (TextView) view.findViewById(R.id.dinner_item_18);
        dinnerItem19 = (TextView) view.findViewById(R.id.dinner_item_19);
        dinnerItem20 = (TextView) view.findViewById(R.id.dinner_item_20);
        dinnerItem21 = (TextView) view.findViewById(R.id.dinner_item_21);
        dinnerPrice1 = (TextView) view.findViewById(R.id.dinner_price_1);
        dinnerPrice2 = (TextView) view.findViewById(R.id.dinner_price_2);
        dinnerPrice3 = (TextView) view.findViewById(R.id.dinner_price_3);
        dinnerPrice4 = (TextView) view.findViewById(R.id.dinner_price_4);
        dinnerPrice5 = (TextView) view.findViewById(R.id.dinner_price_5);
        dinnerPrice6 = (TextView) view.findViewById(R.id.dinner_price_6);
        dinnerPrice7 = (TextView) view.findViewById(R.id.dinner_price_7);
        dinnerPrice8 = (TextView) view.findViewById(R.id.dinner_price_8);
        dinnerPrice9 = (TextView) view.findViewById(R.id.dinner_price_9);
        dinnerPrice10 = (TextView) view.findViewById(R.id.dinner_price_10);
        dinnerPrice11 = (TextView) view.findViewById(R.id.dinner_price_11);
        dinnerPrice12 = (TextView) view.findViewById(R.id.dinner_price_12);
        dinnerPrice13 = (TextView) view.findViewById(R.id.dinner_price_13);
        dinnerPrice14 = (TextView) view.findViewById(R.id.dinner_price_14);
        dinnerPrice15 = (TextView) view.findViewById(R.id.dinner_price_15);
        dinnerPrice16 = (TextView) view.findViewById(R.id.dinner_price_16);
        dinnerPrice17 = (TextView) view.findViewById(R.id.dinner_price_17);
        dinnerPrice18 = (TextView) view.findViewById(R.id.dinner_price_18);
        dinnerPrice19 = (TextView) view.findViewById(R.id.dinner_price_19);
        dinnerPrice20 = (TextView) view.findViewById(R.id.dinner_price_20);
        dinnerPrice21 = (TextView) view.findViewById(R.id.dinner_price_21);

        initRemoteConfig();
        setupView();

        return view;
    }

    private void initRemoteConfig() {
        mRemoteConfig = FirebaseRemoteConfig.getInstance();

        Resources res = getResources();

        HashMap<String, Object> defaults = new HashMap<>();
        defaults.put("Dinner_Item_1", getString(R.string.Dinner_Item_1));
        defaults.put("Dinner_Item_2", getString(R.string.Dinner_Item_2));
        defaults.put("Dinner_Item_3", getString(R.string.Dinner_Item_3));
        defaults.put("Dinner_Item_4", getString(R.string.Dinner_Item_4));
        defaults.put("Dinner_Item_5", getString(R.string.Dinner_Item_5));
        defaults.put("Dinner_Item_6", getString(R.string.Dinner_Item_6));
        defaults.put("Dinner_Item_7", getString(R.string.Dinner_Item_7));
        defaults.put("Dinner_Item_8", getString(R.string.Dinner_Item_8));
        defaults.put("Dinner_Item_9", getString(R.string.Dinner_Item_9));
        defaults.put("Dinner_Item_10", getString(R.string.Dinner_Item_10));
        defaults.put("Dinner_Item_11", getString(R.string.Dinner_Item_11));
        defaults.put("Dinner_Item_12", getString(R.string.Dinner_Item_12));
        defaults.put("Dinner_Item_13", getString(R.string.Dinner_Item_13));
        defaults.put("Dinner_Item_14", getString(R.string.Dinner_Item_14));
        defaults.put("Dinner_Item_15", getString(R.string.Dinner_Item_15));
        defaults.put("Dinner_Item_16", getString(R.string.Dinner_Item_16));
        defaults.put("Dinner_Item_17", getString(R.string.Dinner_Item_17));
        defaults.put("Dinner_Item_18", getString(R.string.Dinner_Item_18));
        defaults.put("Dinner_Item_19", getString(R.string.Dinner_Item_19));
        defaults.put("Dinner_Item_20", getString(R.string.Dinner_Item_20));
        defaults.put("Dinner_Item_21", getString(R.string.Dinner_Item_21));
        defaults.put("Dinner_Desc_1", getString(R.string.Dinner_Desc_1));
        defaults.put("Dinner_Desc_2", getString(R.string.Dinner_Desc_2));
        defaults.put("Dinner_Desc_3", getString(R.string.Dinner_Desc_3));
        defaults.put("Dinner_Desc_4", getString(R.string.Dinner_Desc_4));
        defaults.put("Dinner_Desc_5", getString(R.string.Dinner_Desc_5));
        defaults.put("Dinner_Desc_6", getString(R.string.Dinner_Desc_6));
        defaults.put("Dinner_Desc_7", getString(R.string.Dinner_Desc_7));
        defaults.put("Dinner_Desc_8", getString(R.string.Dinner_Desc_8));
        defaults.put("Dinner_Desc_9", getString(R.string.Dinner_Desc_9));
        defaults.put("Dinner_Desc_10", getString(R.string.Dinner_Desc_10));
        defaults.put("Dinner_Desc_11", getString(R.string.Dinner_Desc_11));
        defaults.put("Dinner_Desc_12", getString(R.string.Dinner_Desc_12));
        defaults.put("Dinner_Desc_13", getString(R.string.Dinner_Desc_13));
        defaults.put("Dinner_Desc_14", getString(R.string.Dinner_Desc_14));
        defaults.put("Dinner_Desc_15", getString(R.string.Dinner_Desc_15));
        defaults.put("Dinner_Desc_16", getString(R.string.Dinner_Desc_16));
        defaults.put("Dinner_Desc_17", getString(R.string.Dinner_Desc_17));
        defaults.put("Dinner_Desc_18", getString(R.string.Dinner_Desc_18));
        defaults.put("Dinner_Desc_19", getString(R.string.Dinner_Desc_19));
        defaults.put("Dinner_Desc_20", getString(R.string.Dinner_Desc_20));
        defaults.put("Dinner_Desc_21", getString(R.string.Dinner_Desc_21));
        defaults.put("Dinner_Price_1", getString(R.string.Dinner_Price_1));
        defaults.put("Dinner_Price_2", getString(R.string.Dinner_Price_2));
        defaults.put("Dinner_Price_3", getString(R.string.Dinner_Price_3));
        defaults.put("Dinner_Price_4", getString(R.string.Dinner_Price_4));
        defaults.put("Dinner_Price_5", getString(R.string.Dinner_Price_5));
        defaults.put("Dinner_Price_6", getString(R.string.Dinner_Price_6));
        defaults.put("Dinner_Price_7", getString(R.string.Dinner_Price_7));
        defaults.put("Dinner_Price_8", getString(R.string.Dinner_Price_8));
        defaults.put("Dinner_Price_9", getString(R.string.Dinner_Price_9));
        defaults.put("Dinner_Price_10", getString(R.string.Dinner_Price_10));
        defaults.put("Dinner_Price_11", getString(R.string.Dinner_Price_11));
        defaults.put("Dinner_Price_12", getString(R.string.Dinner_Price_12));
        defaults.put("Dinner_Price_13", getString(R.string.Dinner_Price_13));
        defaults.put("Dinner_Price_14", getString(R.string.Dinner_Price_14));
        defaults.put("Dinner_Price_15", getString(R.string.Dinner_Price_15));
        defaults.put("Dinner_Price_16", getString(R.string.Dinner_Price_16));
        defaults.put("Dinner_Price_17", getString(R.string.Dinner_Price_17));
        defaults.put("Dinner_Price_18", getString(R.string.Dinner_Price_18));
        defaults.put("Dinner_Price_19", getString(R.string.Dinner_Price_19));
        defaults.put("Dinner_Price_20", getString(R.string.Dinner_Price_20));
        defaults.put("Dinner_Price_21", getString(R.string.Dinner_Price_21));


        mRemoteConfig.setDefaults(defaults);
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mRemoteConfig.setConfigSettings(remoteConfigSettings);
        fetchRemoteConfigValues();
    }

    private void fetchRemoteConfigValues() {
        long cacheExpiration = 3600;

        //expire the cache immediately for development mode.
        if (mRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            // task successful. Activate the fetched data
                            mRemoteConfig.activateFetched();
                            setupView();
                        } else {
                            //task failed
                        }
                    }
                });
    }

    private void setupView() {
        setDinnerItem1();
        setDinnerItem2();
        setDinnerItem3();
        setDinnerItem4();
        setDinnerItem5();
        setDinnerItem6();
        setDinnerItem7();
        setDinnerItem8();
        setDinnerItem9();
        setDinnerItem10();
        setDinnerItem11();
        setDinnerItem12();
        setDinnerItem13();
        setDinnerItem14();
        setDinnerItem15();
        setDinnerItem16();
        setDinnerItem17();
        setDinnerItem18();
        setDinnerItem19();
        setDinnerItem20();
        setDinnerItem21();
        setDinnerDesc1();
        setDinnerDesc2();
        setDinnerDesc3();
        setDinnerDesc4();
        setDinnerDesc5();
        setDinnerDesc6();
        setDinnerDesc7();
        setDinnerDesc8();
        setDinnerDesc9();
        setDinnerDesc10();
        setDinnerDesc11();
        setDinnerDesc12();
        setDinnerDesc13();
        setDinnerDesc14();
        setDinnerDesc15();
        setDinnerDesc16();
        setDinnerDesc17();
        setDinnerDesc18();
        setDinnerDesc19();
        setDinnerDesc20();
        setDinnerDesc21();
        setDinnerPrice1();
        setDinnerPrice2();
        setDinnerPrice3();
        setDinnerPrice4();
        setDinnerPrice5();
        setDinnerPrice6();
        setDinnerPrice7();
        setDinnerPrice8();
        setDinnerPrice9();
        setDinnerPrice10();
        setDinnerPrice11();
        setDinnerPrice12();
        setDinnerPrice13();
        setDinnerPrice14();
        setDinnerPrice15();
        setDinnerPrice16();
        setDinnerPrice17();
        setDinnerPrice18();
        setDinnerPrice19();
        setDinnerPrice20();
        setDinnerPrice21();
    }

    private void setDinnerItem1() {
        String prompt = mRemoteConfig.getString(DinnerItem1);
        if (prompt != null) {
            dinnerItem1.setText(prompt);
        }
    }

    private void setDinnerItem2() {
        String prompt = mRemoteConfig.getString(DinnerItem2);
        if (prompt != null) {
            dinnerItem2.setText(prompt);
        }
    }

    private void setDinnerItem3() {
        String prompt = mRemoteConfig.getString(DinnerItem3);
        if (prompt != null) {
            dinnerItem3.setText(prompt);
        }
    }

    private void setDinnerItem4() {
        String prompt = mRemoteConfig.getString(DinnerItem4);
        if (prompt != null) {
            dinnerItem4.setText(prompt);
        }
    }

    private void setDinnerItem5() {
        String prompt = mRemoteConfig.getString(DinnerItem5);
        if (prompt != null) {
            dinnerItem5.setText(prompt);
        }
    }

    private void setDinnerItem6() {
        String prompt = mRemoteConfig.getString(DinnerItem6);
        if (prompt != null) {
            dinnerItem6.setText(prompt);
        }
    }

    private void setDinnerItem7() {
        String prompt = mRemoteConfig.getString(DinnerItem7);
        if (prompt != null) {
            dinnerItem7.setText(prompt);
        }
    }

    private void setDinnerItem8() {
        String prompt = mRemoteConfig.getString(DinnerItem8);
        if (prompt != null) {
            dinnerItem8.setText(prompt);
        }
    }

    private void setDinnerItem9() {
        String prompt = mRemoteConfig.getString(DinnerItem9);
        if (prompt != null) {
            dinnerItem9.setText(prompt);
        }
    }

    private void setDinnerItem10() {
        String prompt = mRemoteConfig.getString(DinnerItem10);
        if (prompt != null) {
            dinnerItem10.setText(prompt);
        }
    }

    private void setDinnerItem11() {
        String prompt = mRemoteConfig.getString(DinnerItem11);
        if (prompt != null) {
            dinnerItem11.setText(prompt);
        }
    }

    private void setDinnerItem12() {
        String prompt = mRemoteConfig.getString(DinnerItem12);
        if (prompt != null) {
            dinnerItem12.setText(prompt);
        }
    }

    private void setDinnerItem13() {
        String prompt = mRemoteConfig.getString(DinnerItem13);
        if (prompt != null) {
            dinnerItem13.setText(prompt);
        }
    }

    private void setDinnerItem14() {
        String prompt = mRemoteConfig.getString(DinnerItem14);
        if (prompt != null) {
            dinnerItem14.setText(prompt);
        }
    }

    private void setDinnerItem15() {
        String prompt = mRemoteConfig.getString(DinnerItem15);
        if (prompt != null) {
            dinnerItem15.setText(prompt);
        }
    }

    private void setDinnerItem16() {
        String prompt = mRemoteConfig.getString(DinnerItem16);
        if (prompt != null) {
            dinnerItem16.setText(prompt);
        }
    }


    private void setDinnerItem17() {
        String prompt = mRemoteConfig.getString(DinnerItem17);
        if (prompt != null) {
            dinnerItem17.setText(prompt);
        }
    }

    private void setDinnerItem18() {
        String prompt = mRemoteConfig.getString(DinnerItem18);
        if (prompt != null) {
            dinnerItem18.setText(prompt);
        }
    }

    private void setDinnerItem19() {
        String prompt = mRemoteConfig.getString(DinnerItem19);
        if (prompt != null) {
            dinnerItem19.setText(prompt);
        }
    }

    private void setDinnerItem20() {
        String prompt = mRemoteConfig.getString(DinnerItem20);
        if (prompt != null) {
            dinnerItem20.setText(prompt);
        }
    }

    private void setDinnerItem21() {
        String prompt = mRemoteConfig.getString(DinnerItem21);
        if (prompt != null) {
            dinnerItem21.setText(prompt);
        }
    }

    private void setDinnerDesc1() {
        String prompt = mRemoteConfig.getString(DinnerDesc1);
        if (prompt != null) {
            dinnerDesc1.setText(prompt);
        }
    }

    private void setDinnerDesc2() {
        String prompt = mRemoteConfig.getString(DinnerDesc2);
        if (prompt != null) {
            dinnerDesc2.setText(prompt);
        }
    }

    private void setDinnerDesc3() {
        String prompt = mRemoteConfig.getString(DinnerDesc3);
        if (prompt != null) {
            dinnerDesc3.setText(prompt);
        }
    }

    private void setDinnerDesc4() {
        String prompt = mRemoteConfig.getString(DinnerDesc4);
        if (prompt != null) {
            dinnerDesc4.setText(prompt);
        }
    }

    private void setDinnerDesc5() {
        String prompt = mRemoteConfig.getString(DinnerDesc5);
        if (prompt != null) {
            dinnerDesc5.setText(prompt);
        }
    }

    private void setDinnerDesc6() {
        String prompt = mRemoteConfig.getString(DinnerDesc6);
        if (prompt != null) {
            dinnerDesc6.setText(prompt);
        }
    }

    private void setDinnerDesc7() {
        String prompt = mRemoteConfig.getString(DinnerDesc7);
        if (prompt != null) {
            dinnerDesc7.setText(prompt);
        }
    }

    private void setDinnerDesc8() {
        String prompt = mRemoteConfig.getString(DinnerDesc8);
        if (prompt != null) {
            dinnerDesc8.setText(prompt);
        }
    }

    private void setDinnerDesc9() {
        String prompt = mRemoteConfig.getString(DinnerDesc9);
        if (prompt != null) {
            dinnerDesc9.setText(prompt);
        }
    }

    private void setDinnerDesc10() {
        String prompt = mRemoteConfig.getString(DinnerDesc10);
        if (prompt != null) {
            dinnerDesc10.setText(prompt);
        }
    }

    private void setDinnerDesc11() {
        String prompt = mRemoteConfig.getString(DinnerDesc11);
        if (prompt != null) {
            dinnerDesc11.setText(prompt);
        }
    }

    private void setDinnerDesc12() {
        String prompt = mRemoteConfig.getString(DinnerDesc12);
        if (prompt != null) {
            dinnerDesc12.setText(prompt);
        }
    }

    private void setDinnerDesc13() {
        String prompt = mRemoteConfig.getString(DinnerDesc13);
        if (prompt != null) {
            dinnerDesc13.setText(prompt);
        }
    }

    private void setDinnerDesc14() {
        String prompt = mRemoteConfig.getString(DinnerDesc14);
        if (prompt != null) {
            dinnerDesc14.setText(prompt);
        }
    }

    private void setDinnerDesc15() {
        String prompt = mRemoteConfig.getString(DinnerDesc15);
        if (prompt != null) {
            dinnerDesc15.setText(prompt);
        }
    }

    private void setDinnerDesc16() {
        String prompt = mRemoteConfig.getString(DinnerDesc16);
        if (prompt != null) {
            dinnerDesc16.setText(prompt);
        }
    }

    private void setDinnerDesc17() {
        String prompt = mRemoteConfig.getString(DinnerDesc17);
        if (prompt != null) {
            dinnerDesc17.setText(prompt);
        }
    }

    private void setDinnerDesc18() {
        String prompt = mRemoteConfig.getString(DinnerDesc18);
        if (prompt != null) {
            dinnerDesc18.setText(prompt);
        }
    }

    private void setDinnerDesc19() {
        String prompt = mRemoteConfig.getString(DinnerDesc19);
        if (prompt != null) {
            dinnerDesc19.setText(prompt);
        }
    }

    private void setDinnerDesc20() {
        String prompt = mRemoteConfig.getString(DinnerDesc20);
        if (prompt != null) {
            dinnerDesc20.setText(prompt);
        }
    }

    private void setDinnerDesc21() {
        String prompt = mRemoteConfig.getString(DinnerDesc21);
        if (prompt != null) {
            dinnerDesc21.setText(prompt);
        }
    }

    private void setDinnerPrice1() {
        String prompt = mRemoteConfig.getString(DinnerPrice1);
        if (prompt != null) {
            dinnerPrice1.setText(prompt);
        }
    }

    private void setDinnerPrice2() {
        String prompt = mRemoteConfig.getString(DinnerPrice2);
        if (prompt != null) {
            dinnerPrice2.setText(prompt);
        }
    }

    private void setDinnerPrice3() {
        String prompt = mRemoteConfig.getString(DinnerPrice3);
        if (prompt != null) {
            dinnerPrice3.setText(prompt);
        }
    }

    private void setDinnerPrice4() {
        String prompt = mRemoteConfig.getString(DinnerPrice4);
        if (prompt != null) {
            dinnerPrice4.setText(prompt);
        }
    }

    private void setDinnerPrice5() {
        String prompt = mRemoteConfig.getString(DinnerPrice5);
        if (prompt != null) {
            dinnerPrice5.setText(prompt);
        }
    }

    private void setDinnerPrice6() {
        String prompt = mRemoteConfig.getString(DinnerPrice6);
        if (prompt != null) {
            dinnerPrice6.setText(prompt);
        }
    }

    private void setDinnerPrice7() {
        String prompt = mRemoteConfig.getString(DinnerPrice7);
        if (prompt != null) {
            dinnerPrice7.setText(prompt);
        }
    }

    private void setDinnerPrice8() {
        String prompt = mRemoteConfig.getString(DinnerPrice8);
        if (prompt != null) {
            dinnerPrice8.setText(prompt);
        }
    }

    private void setDinnerPrice9() {
        String prompt = mRemoteConfig.getString(DinnerPrice9);
        if (prompt != null) {
            dinnerPrice9.setText(prompt);
        }
    }

    private void setDinnerPrice10() {
        String prompt = mRemoteConfig.getString(DinnerPrice10);
        if (prompt != null) {
            dinnerPrice10.setText(prompt);
        }
    }

    private void setDinnerPrice11() {
        String prompt = mRemoteConfig.getString(DinnerPrice11);
        if (prompt != null) {
            dinnerPrice11.setText(prompt);
        }
    }

    private void setDinnerPrice12() {
        String prompt = mRemoteConfig.getString(DinnerPrice12);
        if (prompt != null) {
            dinnerPrice12.setText(prompt);
        }
    }

    private void setDinnerPrice13() {
        String prompt = mRemoteConfig.getString(DinnerPrice13);
        if (prompt != null) {
            dinnerPrice13.setText(prompt);
        }
    }

    private void setDinnerPrice14() {
        String prompt = mRemoteConfig.getString(DinnerPrice14);
        if (prompt != null) {
            dinnerPrice14.setText(prompt);
        }
    }

    private void setDinnerPrice15() {
        String prompt = mRemoteConfig.getString(DinnerPrice15);
        if (prompt != null) {
            dinnerPrice15.setText(prompt);
        }
    }

    private void setDinnerPrice16() {
        String prompt = mRemoteConfig.getString(DinnerPrice16);
        if (prompt != null) {
            dinnerPrice16.setText(prompt);
        }
    }

    private void setDinnerPrice17() {
        String prompt = mRemoteConfig.getString(DinnerPrice17);
        if (prompt != null) {
            dinnerPrice17.setText(prompt);
        }
    }

    private void setDinnerPrice18() {
        String prompt = mRemoteConfig.getString(DinnerPrice18);
        if (prompt != null) {
            dinnerPrice19.setText(prompt);
        }
    }

    private void setDinnerPrice19() {
        String prompt = mRemoteConfig.getString(DinnerPrice19);
        if (prompt != null) {
            dinnerPrice19.setText(prompt);
        }
    }

    private void setDinnerPrice20() {
        String prompt = mRemoteConfig.getString(DinnerPrice20);
        if (prompt != null) {
            dinnerPrice20.setText(prompt);
        }
    }

    private void setDinnerPrice21() {
        String prompt = mRemoteConfig.getString(DinnerPrice21);
        if (prompt != null) {
            dinnerPrice21.setText(prompt);
        }
    }
}