package com.jenovaprojects.beanscard.updated_ui.menu_fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.Menu;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.MenusAdapter;
import com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters.menu_decorators.DividerItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jenova on 5/20/17.
 */

public class LunchFragment extends Fragment {

    FirebaseRemoteConfig mRemoteConfig;
    private static final String LunchItem1 = "lunch_item_1";
    private static final String LunchItem2 = "lunch_item_2";
    private static final String LunchItem3 = "lunch_item_3";
    private static final String LunchItem4 = "lunch_item_4";
    private static final String LunchItem5 = "lunch_item_5";
    private static final String LunchItem6 = "lunch_item_6";
    private static final String LunchItem7 = "lunch_item_7";
    private static final String LunchItem8 = "lunch_item_8";
    private static final String LunchItem9 = "lunch_item_9";
    private static final String LunchItem10 = "lunch_item_10";
    private static final String LunchItem11 = "lunch_item_11";
    private static final String LunchItem12 = "lunch_item_12";
    private static final String LunchItem13 = "lunch_item_13";
    private static final String LunchItem14 = "lunch_item_14";
    private static final String LunchItem15 = "lunch_item_15";
    private static final String LunchItem16 = "lunch_item_16";

    private static final String LunchDesc1 = "lunch_desc_1";
    private static final String LunchDesc2 = "lunch_desc_2";
    private static final String LunchDesc3 = "lunch_desc_3";
    private static final String LunchDesc4 = "lunch_desc_4";
    private static final String LunchDesc5 = "lunch_desc_5";
    private static final String LunchDesc6 = "lunch_desc_6";
    private static final String LunchDesc7 = "lunch_desc_7";
    private static final String LunchDesc8 = "lunch_desc_8";
    private static final String LunchDesc9 = "lunch_desc_9";
    private static final String LunchDesc10 = "lunch_desc_10";
    private static final String LunchDesc11 = "lunch_desc_11";
    private static final String LunchDesc12 = "lunch_desc_12";
    private static final String LunchDesc13 = "lunch_desc_13";
    private static final String LunchDesc14 = "lunch_desc_14";
    private static final String LunchDesc15 = "lunch_desc_15";
    private static final String LunchDesc16 = "lunch_desc_16";

    private static final String LunchPrice1 = "lunch_price_1";
    private static final String LunchPrice2 = "lunch_price_2";
    private static final String LunchPrice3 = "lunch_price_3";
    private static final String LunchPrice4 = "lunch_price_4";
    private static final String LunchPrice5 = "lunch_price_5";
    private static final String LunchPrice6 = "lunch_price_6";
    private static final String LunchPrice7 = "lunch_price_7";
    private static final String LunchPrice8 = "lunch_price_8";
    private static final String LunchPrice9 = "lunch_price_9";
    private static final String LunchPrice10 = "lunch_price_10";
    private static final String LunchPrice11 = "lunch_price_11";
    private static final String LunchPrice12 = "lunch_price_12";
    private static final String LunchPrice13 = "lunch_price_13";
    private static final String LunchPrice14 = "lunch_price_14";
    private static final String LunchPrice15 = "lunch_price_15";
    private static final String LunchPrice16 = "lunch_price_16";

    TextView lunchItem1, lunchItem2, lunchItem3, lunchItem4, lunchItem5,
            lunchItem6, lunchItem7, lunchItem8, lunchItem9, lunchItem10,
            lunchItem11, lunchItem12, lunchItem13, lunchItem14, lunchItem15, lunchItem16,
            lunchDesc1, lunchDesc2, lunchDesc3, lunchDesc4, lunchDesc5, lunchDesc6, lunchDesc7,
            lunchDesc8, lunchDesc9, lunchDesc10, lunchDesc11, lunchDesc12, lunchDesc13,
            lunchDesc14, lunchDesc15, lunchDesc16, lunchPrice1,
            lunchPrice2, lunchPrice3, lunchPrice4, lunchPrice5, lunchPrice6,
            lunchPrice7, lunchPrice8, lunchPrice9, lunchPrice10, lunchPrice11,
            lunchPrice12, lunchPrice13, lunchPrice14, lunchPrice15, lunchPrice16;

    public static LunchFragment newInstance() {
        LunchFragment fragment = new LunchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lunch, container, false);

        lunchDesc1 = (TextView) view.findViewById(R.id.lunch_desc_1);
        lunchDesc2 = (TextView) view.findViewById(R.id.lunch_desc_2);
        lunchDesc3 = (TextView) view.findViewById(R.id.lunch_desc_3);
        lunchDesc4 = (TextView) view.findViewById(R.id.lunch_desc_4);
        lunchDesc5 = (TextView) view.findViewById(R.id.lunch_desc_5);
        lunchDesc6 = (TextView) view.findViewById(R.id.lunch_desc_6);
        lunchDesc7 = (TextView) view.findViewById(R.id.lunch_desc_7);
        lunchDesc8 = (TextView) view.findViewById(R.id.lunch_desc_8);
        lunchDesc9 = (TextView) view.findViewById(R.id.lunch_desc_9);
        lunchDesc10 = (TextView) view.findViewById(R.id.lunch_desc_10);
        lunchDesc11 = (TextView) view.findViewById(R.id.lunch_desc_11);
        lunchDesc12 = (TextView) view.findViewById(R.id.lunch_desc_12);
        lunchDesc13 = (TextView) view.findViewById(R.id.lunch_desc_13);
        lunchDesc14 = (TextView) view.findViewById(R.id.lunch_desc_14);
        lunchDesc15 = (TextView) view.findViewById(R.id.lunch_desc_15);
        lunchDesc16 = (TextView) view.findViewById(R.id.lunch_desc_16);
        lunchItem1 = (TextView) view.findViewById(R.id.lunch_item_1);
        lunchItem2 = (TextView) view.findViewById(R.id.lunch_item_2);
        lunchItem3 = (TextView) view.findViewById(R.id.lunch_item_3);
        lunchItem4 = (TextView) view.findViewById(R.id.lunch_item_4);
        lunchItem5 = (TextView) view.findViewById(R.id.lunch_item_5);
        lunchItem6 = (TextView) view.findViewById(R.id.lunch_item_6);
        lunchItem7 = (TextView) view.findViewById(R.id.lunch_item_7);
        lunchItem8 = (TextView) view.findViewById(R.id.lunch_item_8);
        lunchItem9 = (TextView) view.findViewById(R.id.lunch_item_9);
        lunchItem10 = (TextView) view.findViewById(R.id.lunch_item_10);
        lunchItem11 = (TextView) view.findViewById(R.id.lunch_item_11);
        lunchItem12 = (TextView) view.findViewById(R.id.lunch_item_12);
        lunchItem13 = (TextView) view.findViewById(R.id.lunch_item_13);
        lunchItem14 = (TextView) view.findViewById(R.id.lunch_item_14);
        lunchItem15 = (TextView) view.findViewById(R.id.lunch_item_15);
        lunchItem16 = (TextView) view.findViewById(R.id.lunch_item_16);
        lunchPrice1 = (TextView) view.findViewById(R.id.lunch_price_1);
        lunchPrice2 = (TextView) view.findViewById(R.id.lunch_price_2);
        lunchPrice3 = (TextView) view.findViewById(R.id.lunch_price_3);
        lunchPrice4 = (TextView) view.findViewById(R.id.lunch_price_4);
        lunchPrice5 = (TextView) view.findViewById(R.id.lunch_price_5);
        lunchPrice6 = (TextView) view.findViewById(R.id.lunch_price_6);
        lunchPrice7 = (TextView) view.findViewById(R.id.lunch_price_7);
        lunchPrice8 = (TextView) view.findViewById(R.id.lunch_price_8);
        lunchPrice9 = (TextView) view.findViewById(R.id.lunch_price_9);
        lunchPrice10 = (TextView) view.findViewById(R.id.lunch_price_10);
        lunchPrice11 = (TextView) view.findViewById(R.id.lunch_price_11);
        lunchPrice12 = (TextView) view.findViewById(R.id.lunch_price_12);
        lunchPrice13 = (TextView) view.findViewById(R.id.lunch_price_13);
        lunchPrice14 = (TextView) view.findViewById(R.id.lunch_price_14);
        lunchPrice15 = (TextView) view.findViewById(R.id.lunch_price_15);
        lunchPrice16 = (TextView) view.findViewById(R.id.lunch_price_16);

        initRemoteConfig();
        setupView();

        return view;
    }

    private void initRemoteConfig() {
        mRemoteConfig = FirebaseRemoteConfig.getInstance();

        Resources res = getResources();

        HashMap<String, Object> defaults = new HashMap<>();
        defaults.put("Lunch_Item_1", getString(R.string.Lunch_Item_1));
        defaults.put("Lunch_Item_2", getString(R.string.Lunch_Item_2));
        defaults.put("Lunch_Item_3", getString(R.string.Lunch_Item_3));
        defaults.put("Lunch_Item_4", getString(R.string.Lunch_Item_4));
        defaults.put("Lunch_Item_5", getString(R.string.Lunch_Item_5));
        defaults.put("Lunch_Item_6", getString(R.string.Lunch_Item_6));
        defaults.put("Lunch_Item_7", getString(R.string.Lunch_Item_7));
        defaults.put("Lunch_Item_8", getString(R.string.Lunch_Item_8));
        defaults.put("Lunch_Item_9", getString(R.string.Lunch_Item_9));
        defaults.put("Lunch_Item_10", getString(R.string.Lunch_Item_10));
        defaults.put("Lunch_Item_11", getString(R.string.Lunch_Item_11));
        defaults.put("Lunch_Item_12", getString(R.string.Lunch_Item_12));
        defaults.put("Lunch_Item_13", getString(R.string.Lunch_Item_13));
        defaults.put("Lunch_Item_14", getString(R.string.Lunch_Item_14));
        defaults.put("Lunch_Item_15", getString(R.string.Lunch_Item_15));
        defaults.put("Lunch_Item_16", getString(R.string.Lunch_Item_16));
        defaults.put("Lunch_Desc_1", getString(R.string.Lunch_Desc_1));
        defaults.put("Lunch_Desc_2", getString(R.string.Lunch_Desc_2));
        defaults.put("Lunch_Desc_3", getString(R.string.Lunch_Desc_3));
        defaults.put("Lunch_Desc_4", getString(R.string.Lunch_Desc_4));
        defaults.put("Lunch_Desc_5", getString(R.string.Lunch_Desc_5));
        defaults.put("Lunch_Desc_6", getString(R.string.Lunch_Desc_6));
        defaults.put("Lunch_Desc_7", getString(R.string.Lunch_Desc_7));
        defaults.put("Lunch_Desc_8", getString(R.string.Lunch_Desc_8));
        defaults.put("Lunch_Desc_9", getString(R.string.Lunch_Desc_9));
        defaults.put("Lunch_Desc_10", getString(R.string.Lunch_Desc_10));
        defaults.put("Lunch_Desc_11", getString(R.string.Lunch_Desc_11));
        defaults.put("Lunch_Desc_12", getString(R.string.Lunch_Desc_12));
        defaults.put("Lunch_Desc_13", getString(R.string.Lunch_Desc_13));
        defaults.put("Lunch_Desc_14", getString(R.string.Lunch_Desc_14));
        defaults.put("Lunch_Desc_15", getString(R.string.Lunch_Desc_15));
        defaults.put("Lunch_Desc_16", getString(R.string.Lunch_Desc_16));
        defaults.put("Lunch_Price_1", getString(R.string.Lunch_Price_1));
        defaults.put("Lunch_Price_2", getString(R.string.Lunch_Price_2));
        defaults.put("Lunch_Price_3", getString(R.string.Lunch_Price_3));
        defaults.put("Lunch_Price_4", getString(R.string.Lunch_Price_4));
        defaults.put("Lunch_Price_5", getString(R.string.Lunch_Price_5));
        defaults.put("Lunch_Price_6", getString(R.string.Lunch_Price_6));
        defaults.put("Lunch_Price_7", getString(R.string.Lunch_Price_7));
        defaults.put("Lunch_Price_8", getString(R.string.Lunch_Price_8));
        defaults.put("Lunch_Price_9", getString(R.string.Lunch_Price_9));
        defaults.put("Lunch_Price_10", getString(R.string.Lunch_Price_10));
        defaults.put("Lunch_Price_11", getString(R.string.Lunch_Price_11));
        defaults.put("Lunch_Price_12", getString(R.string.Lunch_Price_12));
        defaults.put("Lunch_Price_13", getString(R.string.Lunch_Price_13));
        defaults.put("Lunch_Price_14", getString(R.string.Lunch_Price_14));
        defaults.put("Lunch_Price_15", getString(R.string.Lunch_Price_15));
        defaults.put("Lunch_Price_16", getString(R.string.Lunch_Price_16));


        mRemoteConfig.setDefaults(defaults);
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mRemoteConfig.setConfigSettings(remoteConfigSettings);
        fetchRemoteConfigValues();
    }

    private void fetchRemoteConfigValues() {
        long cacheExpiration = 3600;

        //expire the cache immediately for development mode.
        if (mRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        if (task.isSuccessful()) {
                            // task successful. Activate the fetched data
                            mRemoteConfig.activateFetched();
                            setupView();
                        } else {
                            //task failed
                        }
                    }
                });
    }

    private void setupView() {
        setLunchItem1();
        setLunchItem2();
        setLunchItem3();
        setLunchItem4();
        setLunchItem5();
        setLunchItem6();
        setLunchItem7();
        setLunchItem8();
        setLunchItem9();
        setLunchItem10();
        setLunchItem11();
        setLunchItem12();
        setLunchItem13();
        setLunchItem14();
        setLunchItem15();
        setLunchItem16();
        setLunchDesc1();
        setLunchDesc2();
        setLunchDesc3();
        setLunchDesc4();
        setLunchDesc5();
        setLunchDesc6();
        setLunchDesc7();
        setLunchDesc8();
        setLunchDesc9();
        setLunchDesc10();
        setLunchDesc11();
        setLunchDesc12();
        setLunchDesc13();
        setLunchDesc14();
        setLunchDesc15();
        setLunchDesc16();
        setLunchPrice1();
        setLunchPrice2();
        setLunchPrice3();
        setLunchPrice4();
        setLunchPrice5();
        setLunchPrice6();
        setLunchPrice7();
        setLunchPrice8();
        setLunchPrice9();
        setLunchPrice10();
        setLunchPrice11();
        setLunchPrice12();
        setLunchPrice13();
        setLunchPrice14();
        setLunchPrice15();
        setLunchPrice16();
    }

    private void setLunchItem1() {
        String prompt = mRemoteConfig.getString(LunchItem1);
        if (prompt != null) {
            lunchItem1.setText(prompt);
        }
    }

    private void setLunchItem2() {
        String prompt = mRemoteConfig.getString(LunchItem2);
        if (prompt != null) {
            lunchItem2.setText(prompt);
        }
    }

    private void setLunchItem3() {
        String prompt = mRemoteConfig.getString(LunchItem3);
        if (prompt != null) {
            lunchItem3.setText(prompt);
        }
    }

    private void setLunchItem4() {
        String prompt = mRemoteConfig.getString(LunchItem4);
        if (prompt != null) {
            lunchItem4.setText(prompt);
        }
    }

    private void setLunchItem5() {
        String prompt = mRemoteConfig.getString(LunchItem5);
        if (prompt != null) {
            lunchItem5.setText(prompt);
        }
    }

    private void setLunchItem6() {
        String prompt = mRemoteConfig.getString(LunchItem6);
        if (prompt != null) {
            lunchItem6.setText(prompt);
        }
    }

    private void setLunchItem7() {
        String prompt = mRemoteConfig.getString(LunchItem7);
        if (prompt != null) {
            lunchItem7.setText(prompt);
        }
    }

    private void setLunchItem8() {
        String prompt = mRemoteConfig.getString(LunchItem8);
        if (prompt != null) {
            lunchItem8.setText(prompt);
        }
    }

    private void setLunchItem9() {
        String prompt = mRemoteConfig.getString(LunchItem9);
        if (prompt != null) {
            lunchItem9.setText(prompt);
        }
    }

    private void setLunchItem10() {
        String prompt = mRemoteConfig.getString(LunchItem10);
        if (prompt != null) {
            lunchItem10.setText(prompt);
        }
    }

    private void setLunchItem11() {
        String prompt = mRemoteConfig.getString(LunchItem11);
        if (prompt != null) {
            lunchItem11.setText(prompt);
        }
    }

    private void setLunchItem12() {
        String prompt = mRemoteConfig.getString(LunchItem12);
        if (prompt != null) {
            lunchItem12.setText(prompt);
        }
    }
    private void setLunchItem13() {
        String prompt = mRemoteConfig.getString(LunchItem13);
        if (prompt != null) {
            lunchItem13.setText(prompt);
        }
    }
    private void setLunchItem14() {
        String prompt = mRemoteConfig.getString(LunchItem14);
        if (prompt != null) {
            lunchItem14.setText(prompt);
        }
    }
    private void setLunchItem15() {
        String prompt = mRemoteConfig.getString(LunchItem15);
        if (prompt != null) {
            lunchItem15.setText(prompt);
        }
    }
    private void setLunchItem16() {
        String prompt = mRemoteConfig.getString(LunchItem16);
        if (prompt != null) {
            lunchItem16.setText(prompt);
        }
    }

    private void setLunchDesc1() {
        String prompt = mRemoteConfig.getString(LunchDesc1);
        if (prompt != null) {
            lunchDesc1.setText(prompt);
        }
    }

    private void setLunchDesc2() {
        String prompt = mRemoteConfig.getString(LunchDesc2);
        if (prompt != null) {
            lunchDesc2.setText(prompt);
        }
    }
    private void setLunchDesc3() {
        String prompt = mRemoteConfig.getString(LunchDesc3);
        if (prompt != null) {
            lunchDesc3.setText(prompt);
        }
    }
    private void setLunchDesc4() {
        String prompt = mRemoteConfig.getString(LunchDesc4);
        if (prompt != null) {
            lunchDesc4.setText(prompt);
        }
    }
    private void setLunchDesc5() {
        String prompt = mRemoteConfig.getString(LunchDesc5);
        if (prompt != null) {
            lunchDesc5.setText(prompt);
        }
    }
    private void setLunchDesc6() {
        String prompt = mRemoteConfig.getString(LunchDesc6);
        if (prompt != null) {
            lunchDesc6.setText(prompt);
        }
    }
    private void setLunchDesc7() {
        String prompt = mRemoteConfig.getString(LunchDesc7);
        if (prompt != null) {
            lunchDesc7.setText(prompt);
        }
    }
    private void setLunchDesc8() {
        String prompt = mRemoteConfig.getString(LunchDesc8);
        if (prompt != null) {
            lunchDesc8.setText(prompt);
        }
    }
    private void setLunchDesc9() {
        String prompt = mRemoteConfig.getString(LunchDesc9);
        if (prompt != null) {
            lunchDesc9.setText(prompt);
        }
    }
    private void setLunchDesc10() {
        String prompt = mRemoteConfig.getString(LunchDesc10);
        if (prompt != null) {
            lunchDesc10.setText(prompt);
        }
    }
    private void setLunchDesc11() {
        String prompt = mRemoteConfig.getString(LunchDesc11);
        if (prompt != null) {
            lunchDesc11.setText(prompt);
        }
    }
    private void setLunchDesc12() {
        String prompt = mRemoteConfig.getString(LunchDesc12);
        if (prompt != null) {
            lunchDesc12.setText(prompt);
        }
    }
    private void setLunchDesc13() {
        String prompt = mRemoteConfig.getString(LunchDesc13);
        if (prompt != null) {
            lunchDesc13.setText(prompt);
        }
    }
    private void setLunchDesc14() {
        String prompt = mRemoteConfig.getString(LunchDesc14);
        if (prompt != null) {
            lunchDesc14.setText(prompt);
        }
    }
    private void setLunchDesc15() {
        String prompt = mRemoteConfig.getString(LunchDesc15);
        if (prompt != null) {
            lunchDesc15.setText(prompt);
        }
    }
    private void setLunchDesc16() {
        String prompt = mRemoteConfig.getString(LunchDesc16);
        if (prompt != null) {
            lunchDesc16.setText(prompt);
        }
    }

    private void setLunchPrice1() {
        String prompt = mRemoteConfig.getString(LunchPrice1);
        if (prompt != null) {
            lunchPrice1.setText(prompt);
        }
    }
    private void setLunchPrice2() {
        String prompt = mRemoteConfig.getString(LunchPrice2);
        if (prompt != null) {
            lunchPrice2.setText(prompt);
        }
    }

    private void setLunchPrice3() {
        String prompt = mRemoteConfig.getString(LunchPrice3);
        if (prompt != null) {
            lunchPrice3.setText(prompt);
        }
    }

    private void setLunchPrice4() {
        String prompt = mRemoteConfig.getString(LunchPrice4);
        if (prompt != null) {
            lunchPrice4.setText(prompt);
        }
    }

    private void setLunchPrice5() {
        String prompt = mRemoteConfig.getString(LunchPrice5);
        if (prompt != null) {
            lunchPrice5.setText(prompt);
        }
    }

    private void setLunchPrice6() {
        String prompt = mRemoteConfig.getString(LunchPrice6);
        if (prompt != null) {
            lunchPrice6.setText(prompt);
        }
    }
    private void setLunchPrice7() {
        String prompt = mRemoteConfig.getString(LunchPrice7);
        if (prompt != null) {
            lunchPrice7.setText(prompt);
        }
    }

    private void setLunchPrice8() {
        String prompt = mRemoteConfig.getString(LunchPrice8);
        if (prompt != null) {
            lunchPrice8.setText(prompt);
        }
    }

    private void setLunchPrice9() {
        String prompt = mRemoteConfig.getString(LunchPrice9);
        if (prompt != null) {
            lunchPrice9.setText(prompt);
        }
    }

    private void setLunchPrice10() {
        String prompt = mRemoteConfig.getString(LunchPrice10);
        if (prompt != null) {
            lunchPrice10.setText(prompt);
        }
    }

    private void setLunchPrice11() {
        String prompt = mRemoteConfig.getString(LunchPrice11);
        if (prompt != null) {
            lunchPrice11.setText(prompt);
        }
    }

    private void setLunchPrice12() {
        String prompt = mRemoteConfig.getString(LunchPrice12);
        if (prompt != null) {
            lunchPrice12.setText(prompt);
        }
    }
    private void setLunchPrice13() {
        String prompt = mRemoteConfig.getString(LunchPrice13);
        if (prompt != null) {
            lunchPrice13.setText(prompt);
        }
    }
    private void setLunchPrice14() {
        String prompt = mRemoteConfig.getString(LunchPrice14);
        if (prompt != null) {
            lunchPrice14.setText(prompt);
        }
    }
    private void setLunchPrice15() {
        String prompt = mRemoteConfig.getString(LunchPrice15);
        if (prompt != null) {
            lunchPrice15.setText(prompt);
        }
    }
    private void setLunchPrice16() {
        String prompt = mRemoteConfig.getString(LunchPrice16);
        if (prompt != null) {
            lunchPrice16.setText(prompt);
        }
    }

}