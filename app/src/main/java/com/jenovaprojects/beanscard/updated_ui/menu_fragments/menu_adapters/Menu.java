package com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters;

/**
 * Created by jenova on 5/26/17.
 */

public class Menu {
    private String item, price, desc;

    public Menu(){}

    public Menu(String item, String price, String desc) {
        this.item = item;
        this.price = price;
        this.desc = desc;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
