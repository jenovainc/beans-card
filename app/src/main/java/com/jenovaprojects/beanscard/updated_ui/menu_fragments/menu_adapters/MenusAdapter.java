package com.jenovaprojects.beanscard.updated_ui.menu_fragments.menu_adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jenovaprojects.beanscard.R;

import java.util.List;

/**
 * Created by jenova on 5/26/17.
 */

public class MenusAdapter extends RecyclerView.Adapter<MenusAdapter.MyViewHolder> {

    private List<Menu> menusList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView item, price, desc;

        public MyViewHolder(View view) {
            super(view);
            item = (TextView) view.findViewById(R.id.item);
            price = (TextView) view.findViewById(R.id.price);
            desc  = (TextView) view.findViewById(R.id.desc);
        }
    }


    public MenusAdapter(List<Menu> menusList) {
        this.menusList = menusList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Menu menu = menusList.get(position);
        holder.item.setText(menu.getItem());
        holder.price.setText(menu.getPrice());
        holder.desc.setText(menu.getDesc());
    }

    @Override
    public int getItemCount() {
        return menusList.size();
    }
}