package com.jenovaprojects.beanscard.updated_ui.messaging;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by jenova on 5/29/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    private DatabaseReference mDatabase;

    private String mUserId;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    @Override
    public void onTokenRefresh() {

//Getting registration token
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();

            sendRegistrationToServer(refreshedToken);

            //Displaying token on logcat
            Log.d(TAG, "Refreshed token: " + refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project

           mDatabase.child("FCMtoken").push().setValue(token);

    }
}