package com.jenovaprojects.beanscard.updated_ui.settings;


import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.HomeActivity;
import com.jenovaprojects.beanscard.updated_ui.database.Points;
import com.jenovaprojects.beanscard.updated_ui.database.Profile;
import com.jenovaprojects.beanscard.updated_ui.login.LoginActivity;
import com.jenovaprojects.beanscard.updated_ui.settings.settings_fragments.AccountSettingsFragment;
import com.jenovaprojects.beanscard.updated_ui.settings.settings_fragments.ProfileSettingsFragment;

public class AccountSettings extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseInstanceId mFirebaseInstId;
    private FirebaseAuth auth;

    Button account, profile, link;
    TextView emailId, userId, nameId;
    String UserId, EmailId, displayName, NameId, mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(AccountSettings.this, HomeActivity.class));
            finish();
        } else {
            UserId = mFirebaseUser.getUid();
            EmailId = mFirebaseUser.getEmail();
            NameId = mFirebaseUser.getDisplayName();


            emailId = (TextView) findViewById(R.id.emailId);
            emailId.setText(EmailId);
            userId = (TextView) findViewById(R.id.userId);
            userId.setText(UserId);
            nameId = (TextView) findViewById(R.id.nameId);
            nameId.setText(NameId);

        }

       // newEmail = (EditText) findViewById(R.id.changeEmailET);
       // newPassword = (EditText) findViewById(R.id.changePassET);

        profile = (Button) findViewById(R.id.profileBtn);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RelativeLayout)findViewById(R.id.rootView)).setVisibility(View.INVISIBLE);

                ProfileSettingsFragment fragment = new ProfileSettingsFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        account = (Button) findViewById(R.id.accountBtn);
        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RelativeLayout)findViewById(R.id.rootView)).setVisibility(View.INVISIBLE);

                AccountSettingsFragment fragment = new AccountSettingsFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        link = (Button) findViewById(R.id.linkCardBtn);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountSettings.this, LinkCardActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();


        if (fm.getBackStackEntryCount() > 0) {
            //view.setVisibility(View.VISIBLE);
            ((RelativeLayout)findViewById(R.id.rootView)).setVisibility(View.VISIBLE);
        }

        super.onBackPressed();
    }


}
