package com.jenovaprojects.beanscard.updated_ui.settings;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jenovaprojects.beanscard.R;

import ernestoyaquello.com.verticalstepperform.VerticalStepperFormLayout;
import ernestoyaquello.com.verticalstepperform.interfaces.VerticalStepperForm;

public class LinkCardActivity extends AppCompatActivity implements VerticalStepperForm {

    private VerticalStepperFormLayout verticalStepperForm;

    EditText cardNumber;
    TextView verifyBtn, linkBtn;

    public static final String STATE_NUMBER = "card number";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_card);

        String[] mySteps = {"Card Number", "Verify"};
        int colorPrimary = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary);
        int colorPrimaryDark = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark);

        // Finding the view
        verticalStepperForm = (VerticalStepperFormLayout) findViewById(R.id.vertical_stepper_form);

        // Setting up and initializing the form
        VerticalStepperFormLayout.Builder.newInstance(verticalStepperForm, mySteps, this, this)
                .primaryColor(colorPrimary)
                .primaryDarkColor(colorPrimaryDark)
                .displayBottomNavigation(false) // It is true by default, so in this case this line is not necessary
                .init();
    }

    @Override
    public View createStepContentView(int stepNumber) {
        View view = null;
        switch (stepNumber) {
            case 0:
                view = createCardNumberStep();
                break;
            case 1:
                view = createVerifyStep();
                break;
        }
        return view;
    }

    private View createCardNumberStep() {
        // Here we generate programmatically the view that will be added by the system to the step content layout
        cardNumber = new EditText(this);
        cardNumber.setSingleLine(true);
        cardNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        cardNumber.setHint("Enter the Card Number");
        cardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkCardNumber(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
        cardNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(checkCardNumber(v.getText().toString())) {
                    verticalStepperForm.goToNextStep();
                }
                return false;
            }
        });


        return cardNumber;
    }

    private View createVerifyStep() {
        // In this case we generate the view by inflating a XML file
       verifyBtn = new TextView(this);
       verifyBtn.setText(verify);
       verifyBtn.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        return verifyBtn;
    }


    @Override
    public void onStepOpening(int stepNumber) {
        switch (stepNumber) {
            case 0:
                //checkCardNumber();
                verticalStepperForm.setStepAsCompleted(0);
                break;
            case 1:
                verticalStepperForm.setStepAsCompleted(1);
                break;

        }
    }

    @Override
    public void sendData() {

    }

    private boolean checkCardNumber(String verify) {
        //TODO talk with Keith about verifying card in database

        boolean numberIsCorrect = false;

        if(verify.length() >= 6) {
            numberIsCorrect = true;

            verticalStepperForm.setActiveStepAsCompleted();
            // Equivalent to: verticalStepperForm.setStepAsCompleted(TITLE_STEP_NUM);

        } else {
            String titleErrorString = "error";
            String titleError = String.format(titleErrorString, "must be min 6 numbers");

            verticalStepperForm.setActiveStepAsUncompleted(titleError);
            // Equivalent to: verticalStepperForm.setStepAsUncompleted(TITLE_STEP_NUM, titleError);

        }

        return numberIsCorrect;
    }



}
