package com.jenovaprojects.beanscard.updated_ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.HomeActivity;

public class MoreInfoActivity extends AppCompatActivity {

   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MoreInfoActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }*/

   /* @Override
    protected MaterialAboutList getMaterialAboutList(Context context) {
        MaterialAboutCard.Builder appCardBuilder = new MaterialAboutCard.Builder();
        appCardBuilder.title("Points");
        appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("Get 2 Points for every Dollar spent (pre tax)")
                .icon(R.drawable.ic_redeem)
                .build());
        appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("65 Points")
                .subText("Here you can redeem for a drip coffee or tea of any size, or choose from one of our bottled beverages")
                .icon(R.drawable.ic_local_cafe)
                .build());
        appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("115 Points")
                .subText("Here you can redeem for any of our espresso based drinks, chai latte or a scottish mist of any size")
                .icon(R.drawable.ic_local_cafe)
                .build());
        appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("275 Points")
                .subText("Here you can reddem for anything off of our lunch menu")
                .icon(R.drawable.ic_restaurant)
                .build());
        appCardBuilder.addItem(new MaterialAboutActionItem.Builder()
                .text("425 Points")
                .subText("Time for dinner! you can redeem for anything off of our dinner menu")
                .icon(R.drawable.ic_restaurant)
                .build());

        MaterialAboutCard.Builder aboutCard = new MaterialAboutCard.Builder();
        aboutCard.title("About");
        aboutCard.addItem(new MaterialAboutActionItem.Builder()
                .text("Powered by")
                .subText("Jenova Projects")
                .icon(R.drawable.ic_copyright)
                .build());
        return new MaterialAboutList(appCardBuilder.build(), aboutCard.build());
    } */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

//        Call this method to let the scrollbar scroll off screen
//        setScrollToolbar(true);
    }

}
