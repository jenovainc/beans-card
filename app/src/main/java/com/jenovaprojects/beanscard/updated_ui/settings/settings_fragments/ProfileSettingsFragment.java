package com.jenovaprojects.beanscard.updated_ui.settings.settings_fragments;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jenovaprojects.beanscard.R;
import com.jenovaprojects.beanscard.updated_ui.HomeActivity;
import com.jenovaprojects.beanscard.updated_ui.database.Profile;

/**
 * Created by robmi on 7/7/2017.
 */

public class ProfileSettingsFragment extends android.support.v4.app.Fragment {

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseInstanceId mFirebaseInstId;
    private FirebaseAuth auth;

    TextView displayNameTV;
    LinearLayout displayLayout;

    String UserId, EmailId, displayName, NameId, mUserId;

    public static ProfileSettingsFragment newInstance() {
        ProfileSettingsFragment fragment = new ProfileSettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_settings, container, false);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            startActivity(new Intent(getActivity(), HomeActivity.class));
        } else {
            UserId = mFirebaseUser.getUid();
            EmailId = mFirebaseUser.getEmail();
            NameId = mFirebaseUser.getDisplayName();

            displayNameTV = (TextView) view.findViewById(R.id.displayNameTV);
            displayNameTV.setText(NameId);


        }

        displayLayout = (LinearLayout) view.findViewById(R.id.displayNameLayout);
        displayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });

        return view;

    }

    public void updateProfile(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_profile_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edtName = (EditText) dialogView.findViewById(R.id.profileNameEt);
        final String userName = edtName.getText().toString();

        dialogBuilder.setTitle("Update your Profile");
        dialogBuilder.setMessage("Enter your details here");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                        .setDisplayName(edtName.getText().toString())
                        .build();
                user.updateProfile(profileUpdate)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {

                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                auth = FirebaseAuth.getInstance();
                                mFirebaseUser = auth.getCurrentUser();
                                mFirebaseInstId = FirebaseInstanceId.getInstance();
                                mUserId = mFirebaseUser.getUid();
                                Profile profile = new Profile(edtName.getText().toString());
                                DatabaseReference db_receipt = FirebaseDatabase.getInstance().getReference().child("user").child(mUserId);
                                db_receipt.child("profile").push().setValue(profile);
                            }
                        });

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
